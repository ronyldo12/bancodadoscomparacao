<?php

namespace Objeto;

/**
 * Description of PrepararSql
 *
 * @author leona_000
 */
class SqlDiferencaEntreDuasTabela {

    private $banco1;
    private $banco2;
    private $tabelas1 = array();
    private $tabelas2 = array();

    public function __construct(ConexaoConfiguracao $conexaoBanco1, ConexaoConfiguracao $conexaoBanco2) {
        $this->banco1 = new \mysqli($conexaoBanco1->getServidor(), $conexaoBanco1->getUsuario(), $conexaoBanco1->getSenha(), $conexaoBanco1->getBanco());
        $this->banco2 = new \mysqli($conexaoBanco2->getServidor(), $conexaoBanco2->getUsuario(), $conexaoBanco2->getSenha(), $conexaoBanco2->getBanco());
    }

    public function getSql() {
        $this->_carregarTabelas();

        $sql = array();
        foreach ($this->tabelas1 as $nomeTabela => $campos) {
            if (isset($this->tabelas2[$nomeTabela])) {
                $sqlExecutar = $this->_getSqlDaTabela($nomeTabela, $campos, $this->tabelas2[$nomeTabela]);
                if ($sqlExecutar) {
                    $sql = array_merge($sql,$sqlExecutar);
                }
            }else{
               $tabela =  $this->banco1->query("SHOW CREATE TABLE ".$nomeTabela)->fetch_array();
                $sql[] = $tabela[1].";";
            }
        }
        return $sql;
    }

    /**
     * 
     * @param type $nomeTabela
     * @param type $camposTabela1
     * @param type $camposTabela2
     * @return type
     */
    private function _getSqlDaTabela($nomeTabela, $camposTabela1, $camposTabela2) {
        //print_r($camposTabela2);
        $sql = array();
        $campoAnterior = '';
        foreach ($camposTabela1 as $campo) {


            //SE O CAMPO NÃO EXISTIR
            if (!array_key_exists($campo['name'], $camposTabela2)) {
                $notNull = $campo['null'] == 'NO' ? ' not null ' : '';
                $primaryKey = $campo['key'] == 'PRI' ? ' primary key ' : '';
                $autoIncrement = $campo['extra'] == 'auto_increment' ? ' auto_increment ' : '';
                $comentario = $campo['comments'] ? " COMMENT '{$campo['comments']}' " : '';
                $padrao = $campo['default'] ? " default '{$campo['default']}' " : '';

                $after = '';
                if ($campoAnterior) {
                    $after = " AFTER `{$campoAnterior['name']}`";
                }
                $sql[] = "ALTER TABLE `{$nomeTabela}` ADD COLUMN `{$campo['name']}` {$campo['type']} {$notNull}{$padrao}{$primaryKey}{$autoIncrement}{$comentario}{$after}; ";
                $campoAnterior = $campo;
                continue;
            }


            //Mudança na estrutura
            if ($this->_temMudanca($campo, $camposTabela2[$campo['name']])) {
                $notNull = $campo['null'] == 'NO' ? ' not null ' : '';
                $primaryKey = $campo['key'] == 'PRI' ? ' primary key ' : '';
                $autoIncrement = $campo['extra'] == 'auto_increment' ? ' auto_increment ' : '';
                $comentario = $campo['comments'] ? " COMMENT '{$campo['comments']}' " : '';
                $padrao = $campo['default'] ? " default '{$campo['default']}' " : '';

                $after = '';
                if ($campoAnterior) {
                    $after = " AFTER `{$campoAnterior['name']}`";
                }
                $sql[] = "ALTER TABLE `{$nomeTabela}` CHANGE COLUMN `{$campo['name']}` `{$campo['name']}` {$campo['type']} {$notNull}{$padrao}{$primaryKey}{$autoIncrement}{$comentario}{$after}; ";
                $campoAnterior = $campo;
                continue;
            }

            $campoAnterior = $campo;
        }
        return $sql;
    }

    private function _temMudanca($coluna1, $coluna2) {
        if ($coluna1['type'] != $coluna2['type']) {
            return true;
        }
        if ($coluna1['null'] != $coluna2['null']) {
            return true;
        }
        if ($coluna1['key'] != $coluna2['key']) {
            return true;
        }
        if ($coluna1['default'] != $coluna2['default']) {
            return true;
        }
        if ($coluna1['extra'] != $coluna2['extra']) {
            return true;
        }
        if ($coluna1['comments'] != $coluna2['comments']) {
            return true;
        }

        return false;
    }

    /**
     * 
     */
    private function _carregarTabelas() {
        $tabelas = $this->banco1->query("SHOW TABLES")->fetch_all();
        foreach ($tabelas as $tabela) {
            $tabela = is_array($tabela) ? current($tabela) : $tabela;
            $colunas = $this->banco1->query("SHOW FULL COLUMNS FROM " . $tabela)->fetch_all();
            foreach ($colunas as $k => $coluna) {
                $colunas[$coluna[0]] = $this->_converterCampoParaCampo($coluna);
                unset($colunas[$k]);
            }
            $this->tabelas1[$tabela] = $colunas;
        }

        $tabelas = $this->banco2->query("SHOW TABLES")->fetch_all();
        foreach ($tabelas as $tabela) {
            $tabela = is_array($tabela) ? current($tabela) : $tabela;
            $colunas = $this->banco2->query("SHOW FULL COLUMNS FROM " . $tabela)->fetch_all();
            foreach ($colunas as $k => $coluna) {
                $colunas[$coluna[0]] = $this->_converterCampoParaCampo($coluna);
                unset($colunas[$k]);
            }
            $this->tabelas2[$tabela] = $colunas;
        }
    }

    private function _converterCampoParaCampo($campos) {
        return array(
            'name' => $campos[0],
            'type' => $campos[1],
            'collation' => $campos[2],
            'null' => $campos[3],
            'key' => $campos[4],
            'default' => $campos[5],
            'extra' => $campos[6],
            'privilleges' => $campos[7],
            'comments' => $campos[8],
        );
    }

}
