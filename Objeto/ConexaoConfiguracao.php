<?php

namespace Objeto;

/**
 * Description of ConexaoConfiguracao
 *
 * @author Ronyldo12
 */
class ConexaoConfiguracao {

    private $servidor='localhost';
    private $usuario='root';
    private $senha='';
    private $banco='';

    function getServidor() {
        return $this->servidor;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getSenha() {
        return $this->senha;
    }

    function getBanco() {
        return $this->banco;
    }

    function setServidor($servidor) {
        $this->servidor = $servidor;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setBanco($banco) {
        $this->banco = $banco;
    }

}
